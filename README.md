## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository with GraphQL code first approach. Goal of this repo is to determine how convenient it is to use GraphQL with NestJS and what the pros and cons specifically in terms of maintainability and extendibility versus rest API.

## Dependencies

You need to have [Docker Desktop](https://www.docker.com/products/docker-desktop/) installed in order to spin up the mongo database

## Installation

```bash
$ npm install
```

## Running the app

```bash

# development
$ npm run database:up
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## License

Nest is [MIT licensed](LICENSE).

## TODO

- setup controllers and dtos
- setup e2e testing
- setup unit testing
- extend client model
- create contactHistory model

## CONS

**Problem:** extending classes using the simple extend keyword does not copy the GraphQL decorators, so you need to declare them again, which may inhibit maintainability
**Solution:** possibly need to use abstract classes and instantiate through super, as well as using graphQL interfaces as described in NestJs Docs`

**Questions:**

- How to use a class that can be both an argsType as well as inputType. Can you use both decorators at the same time
- How to e2e test: cannot use simple webserver
