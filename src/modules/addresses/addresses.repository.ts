import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { GetAddress, Address } from './interfaces';
import { AddressDocument, AddressModel } from './schemas/address.model';
@Injectable()
export class AddressesRepository {
  constructor(
    @InjectModel(AddressModel.name)
    private readonly model: Model<AddressDocument>,
  ) {}

  public async getById(id: string): Promise<GetAddress | null> {
    return this.model.findById(id);
  }

  public async getByClientId(clientId: string): Promise<GetAddress | null> {
    return this.model.findOne({ clientId });
  }

  public async create(address: Address): Promise<GetAddress> {
    // FIXME: resolve using virtuals?
    const doc = await this.model.create(address);
    return {
      id: doc._id,
      clientId: doc.clientId,
      street: doc.street,
      streetNumber: doc.streetNumber,
      streetNumberExtension: doc.streetNumberExtension,
      zipCode: doc.zipCode,
      city: doc.city,
      country: doc.country,
    };
  }
}
