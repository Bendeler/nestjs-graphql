import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Address } from '../interfaces/persistent';

export type AddressDocument = Document & AddressModel;

@Schema({ collection: 'Addresses', strict: 'throw' })
export class AddressModel implements Address {
  @Prop({ required: true })
  clientId!: string;

  @Prop({ required: true })
  street!: string;

  @Prop({ required: true })
  streetNumber!: string;

  @Prop()
  streetNumberExtension?: string;

  @Prop({ required: true })
  zipCode!: string;

  @Prop({ required: true })
  city!: string;

  @Prop({ required: true })
  country!: string;
}

export const AddressSchema = SchemaFactory.createForClass(AddressModel);
