import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AddressesRepository } from './addresses.repository';
import { AddressesResolver } from './addresses.resolver';
import { AddresssesService } from './addresses.service';
import { AddressModel, AddressSchema } from './schemas/address.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: AddressModel.name, schema: AddressSchema },
    ]),
  ],
  providers: [AddressesRepository, AddresssesService, AddressesResolver],
  exports: [AddresssesService],
})
export class AddressesModule {}
