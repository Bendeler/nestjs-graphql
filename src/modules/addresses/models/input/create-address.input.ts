import { Field, InputType } from '@nestjs/graphql';
import { IsOptional, IsString } from 'class-validator';
import { Address } from '../../interfaces';

@InputType()
export class CreateAddressInput implements Omit<Address, 'clientId'> {
  @Field()
  @IsString()
  street: string;

  @Field()
  @IsString()
  streetNumber: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  streetNumberExtension?: string;

  @Field()
  @IsString()
  zipCode: string;

  @Field()
  @IsString()
  city: string;

  @Field()
  @IsString()
  country: string;
}
