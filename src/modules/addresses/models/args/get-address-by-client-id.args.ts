import { ArgsType, Field } from '@nestjs/graphql';
import { GetAddressByClientId } from '../../interfaces';

@ArgsType()
export class GetAddressByClientIdArgs implements GetAddressByClientId {
  @Field()
  clientId: string;
}
