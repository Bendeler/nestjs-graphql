import { ArgsType, Field } from '@nestjs/graphql';
import { GetAddressById } from '../../interfaces';

@ArgsType()
export class GetAddressByIdArgs implements GetAddressById {
  @Field()
  id: string;
}
