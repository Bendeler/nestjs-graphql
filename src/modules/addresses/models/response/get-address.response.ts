import { Field, ObjectType } from '@nestjs/graphql';
import { IsOptional, IsString } from 'class-validator';
import { GetAddress } from '../../interfaces';

@ObjectType()
export class GetAddressResponse implements GetAddress {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  clientId: string;

  @Field()
  @IsString()
  street: string;

  @Field()
  @IsString()
  streetNumber: string;

  @Field({ nullable: true })
  @IsString()
  @IsOptional()
  streetNumberExtension?: string;

  @Field()
  @IsString()
  zipCode: string;

  @Field()
  @IsString()
  city: string;

  @Field()
  @IsString()
  country: string;
}
