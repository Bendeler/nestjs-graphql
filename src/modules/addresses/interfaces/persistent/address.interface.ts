export interface Address {
  clientId: string;
  street: string;
  streetNumber: string;
  streetNumberExtension?: string;
  zipCode: string;
  city: string;
  country: string;
}
