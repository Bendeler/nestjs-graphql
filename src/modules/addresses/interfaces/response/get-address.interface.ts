import { Address } from '../persistent';

export interface GetAddress extends Address {
  id: string;
}
