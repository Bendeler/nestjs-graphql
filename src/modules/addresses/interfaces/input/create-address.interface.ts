import { Address } from '../persistent';

export type CreateAddress = Omit<Address, 'clientId'>;
