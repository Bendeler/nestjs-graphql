import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';
import {
  GetAddressResponse,
  GetAddressByClientIdArgs,
  CreateAddressInput,
  GetAddressByIdArgs,
} from './models';
import { AddresssesService } from './addresses.service';

@Resolver(() => GetAddressResponse)
export class AddressesResolver {
  constructor(private readonly service: AddresssesService) {}

  @Query(() => GetAddressResponse, {
    nullable: true,
    name: 'GetAddressByClientId',
  })
  public async getByClientId(
    @Args() { clientId }: GetAddressByClientIdArgs,
  ): Promise<GetAddressResponse | null> {
    return this.service.getByClientId(clientId);
  }

  @Query(() => GetAddressResponse, {
    nullable: true,
    name: 'GetAddressById',
  })
  public async getById(
    @Args() { id }: GetAddressByIdArgs,
  ): Promise<GetAddressResponse | null> {
    return this.service.getById(id);
  }
}
