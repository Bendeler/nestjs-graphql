import { Injectable } from '@nestjs/common';
import { AddressesRepository } from './addresses.repository';
import { CreateAddress, GetAddress } from './interfaces';

@Injectable()
export class AddresssesService {
  constructor(private readonly repository: AddressesRepository) {}

  public async getById(id: string): Promise<GetAddress | null> {
    return this.repository.getById(id);
  }

  public async getByClientId(clientId: string): Promise<GetAddress | null> {
    return this.repository.getByClientId(clientId);
  }

  public async create(
    request: CreateAddress,
    clientId: string,
  ): Promise<GetAddress> {
    return this.repository.create({ ...request, clientId });
  }
}
