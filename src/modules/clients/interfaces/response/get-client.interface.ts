import { GetAddress } from '../../../addresses/interfaces';
import { Client } from '../persistent';

export interface GetClient extends Client {
  id: string;
  address?: GetAddress;
}
