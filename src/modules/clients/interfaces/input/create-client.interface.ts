import { CreateAddress } from '../../../addresses/interfaces';
import { Client } from '../persistent';

export interface CreateClient extends Client {
  address?: CreateAddress;
}
