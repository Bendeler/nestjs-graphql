import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateClient, GetClient } from './interfaces';
import { Client } from './interfaces/persistent';
import { ClientDocument, ClientModel } from './schemas/client.model';
@Injectable()
export class ClientsRepository {
  constructor(
    @InjectModel(ClientModel.name)
    private readonly model: Model<ClientDocument>,
  ) {}

  public async getAll(): Promise<GetClient[]> {
    return this.model.find({});
  }

  public async getById(id: string): Promise<GetClient | null> {
    return this.model.findById(id);
  }

  public async create(requestArgs: CreateClient): Promise<ClientDocument> {
    const newClient: Client = {
      firstName: requestArgs.firstName,
      lastName: requestArgs.lastName,
    };
    return this.model.create(newClient);
  }
}
