import { Injectable } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { AddresssesService } from '../addresses/addresses.service';
import { ClientsRepository } from './clients.repository';
import { CreateClient } from './interfaces';
import { GetClientResponse } from './models';

@Injectable()
export class ClientsService {
  constructor(
    private readonly repository: ClientsRepository,
    private readonly addressesService: AddresssesService,
  ) {}

  public async getAll(): Promise<GetClientResponse[]> {
    return this.repository.getAll();
  }

  public async getById(id: string): Promise<GetClientResponse | null> {
    return this.repository.getById(id);
  }

  public async createClient(
    client: CreateClient,
  ): Promise<GetClientResponse | null> {
    const clientDoc = await this.repository.create(client);
    if (!client.address) return plainToClass(GetClientResponse, clientDoc);
    return plainToClass(GetClientResponse, {
      ...clientDoc,
      id: clientDoc.id,
      address: await this.addressesService.create(
        client.address,
        clientDoc._id,
      ),
    });
  }
}
