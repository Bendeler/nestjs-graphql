import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Client } from '../interfaces/persistent';

export type ClientDocument = Document & ClientModel;

@Schema({ collection: 'Clients', strict: 'throw' })
export class ClientModel implements Client {
  @Prop()
  firstName: string;

  @Prop()
  lastName: string;
}

export const ClientSchema = SchemaFactory.createForClass(ClientModel);
