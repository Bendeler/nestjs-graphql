import { Field, ObjectType } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import {
  IsOptional,
  IsString,
  Validate,
  ValidateNested,
} from 'class-validator';
import { GetAddressResponse } from '../../../../modules/addresses/models';
import { GetClient } from '../../interfaces';

@ObjectType()
export class GetClientResponse implements GetClient {
  @Field()
  @IsString()
  id: string;

  @Field()
  @IsString()
  firstName: string;

  @Field()
  @IsString()
  lastName: string;

  @Field(() => GetAddressResponse)
  @Type(() => GetAddressResponse)
  @ValidateNested()
  @IsOptional()
  address?: GetAddressResponse;
}
