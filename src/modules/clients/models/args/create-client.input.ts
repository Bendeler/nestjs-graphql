import { ArgsType, Field } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import { IsString, ValidateNested } from 'class-validator';
import { CreateAddressInput } from '../../../addresses/models';
import { CreateClient } from '../../interfaces';

@ArgsType()
export class CreateClientInput implements CreateClient {
  @Field()
  @IsString()
  firstName: string;

  @Field()
  @IsString()
  lastName: string;

  @Field(() => CreateAddressInput, { nullable: true })
  @Type(() => CreateAddressInput)
  @ValidateNested()
  address?: CreateAddressInput;
}
