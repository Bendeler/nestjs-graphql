import { ArgsType, Field } from '@nestjs/graphql';
import { IsMongoId } from 'class-validator';
import { GetClientById } from '../../interfaces';

@ArgsType()
export class GetClientByIdArgs implements GetClientById {
  @Field()
  @IsMongoId()
  id: string;
}
