import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { AddresssesService } from '../addresses/addresses.service';
import { GetAddressByIdArgs, GetAddressResponse } from '../addresses/models';
import { ClientsService } from './clients.service';
import { CreateClientInput, GetClientResponse } from './models';

@Resolver(() => GetClientResponse)
export class ClientsResolver {
  constructor(
    private readonly clientsService: ClientsService,
    private readonly addressesService: AddresssesService,
  ) {}

  @Query(() => [GetClientResponse], { name: 'Clients' })
  public async getClients(): Promise<GetClientResponse[]> {
    return this.clientsService.getAll();
  }

  @Query(() => GetClientResponse, { name: 'Client', nullable: true })
  public async getById(
    @Args() { id }: GetAddressByIdArgs,
  ): Promise<GetClientResponse> {
    return this.clientsService.getById(id);
  }

  @ResolveField('address', () => GetAddressResponse, {
    nullable: true,
  })
  public async getAddress(
    @Parent() { id }: GetClientResponse,
  ): Promise<GetAddressResponse | null> {
    return this.addressesService.getByClientId(id);
  }

  @Mutation(() => GetClientResponse, { name: 'CreateClient' })
  public async createClient(
    @Args() request: CreateClientInput,
  ): Promise<GetClientResponse> {
    return this.clientsService.createClient(request);
  }
}
