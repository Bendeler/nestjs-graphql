import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AddressesModule } from '../addresses/addresses.module';
import {
  AddressModel,
  AddressSchema,
} from '../addresses/schemas/address.model';
import { ClientsRepository } from './clients.repository';
import { ClientsResolver } from './clients.resolver';
import { ClientsService } from './clients.service';
import { ClientModel, ClientSchema } from './schemas/client.model';

@Module({
  imports: [
    AddressesModule,
    MongooseModule.forFeature([
      { name: ClientModel.name, schema: ClientSchema },
    ]),
  ],
  providers: [ClientsRepository, ClientsService, ClientsResolver],
})
export class ClientsModule {}
