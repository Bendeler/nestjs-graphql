import { INestApplication } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

export const setupSwaggerConfig = (app: INestApplication): void => {
  const documentBuilder = new DocumentBuilder()
    .setTitle('GraphQL')
    .setDescription('Api related to clients')
    .setVersion('1.0')
    .addTag('clients');

  const doc = SwaggerModule.createDocument(app, documentBuilder.build());
  SwaggerModule.setup('docs', app, doc);
};
