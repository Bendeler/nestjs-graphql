db = db.getSiblingDB('local_db');
db.createUser({
  user: 'local-root',
  pwd: 'local-root-password',
  roles: [{ role: 'readWrite', db: 'local_db' }],
});
db.createCollection('Clients');
